package com.pierreduchemin.soupoftheday.data.network.model

import pl.droidsonroids.jspoon.annotation.Selector

class MenuPage {
    @Selector(".post")
    var weekMenus: List<WeekMenu> = ArrayList()
}