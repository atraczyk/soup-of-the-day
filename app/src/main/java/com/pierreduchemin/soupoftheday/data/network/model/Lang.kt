package com.pierreduchemin.soupoftheday.data.network.model

enum class Lang {
    FR, EN;

    override fun toString(): String {
        return this.name.toLowerCase()
    }
    companion object {
        fun getFromDeviceLang(lang: String?): Lang {
            if (lang == null)
                return EN
            if (lang == "fr")
                return FR
            return EN
        }
    }
}