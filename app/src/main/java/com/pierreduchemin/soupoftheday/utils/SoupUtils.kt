package com.pierreduchemin.soupoftheday.utils

import java.text.DateFormatSymbols
import java.util.*

fun getWeekdayIndice(): Int {
    val calendar = Calendar.getInstance()
    return calendar.get(Calendar.DAY_OF_WEEK)
}

fun getWeekdays(): Array<out String>? {
    return DateFormatSymbols.getInstance().weekdays
}

fun containsWeekdayPattern(text: String): Boolean {
    if (text.length > 100 || text.contains('*'))
        return false

    for (wd in getWeekdays()!!) {
        if (text.contains("$wd:")) {
            return true
        }
    }
    return false
}