package com.pierreduchemin.soupoftheday.ui

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pierreduchemin.soupoftheday.R
import com.pierreduchemin.soupoftheday.data.network.model.WeekMenu
import kotlinx.android.synthetic.main.menu_list_layout.view.*
import java.util.ArrayList

class MenuAdapter(var weekMenus: List<WeekMenu> = ArrayList()) : RecyclerView.Adapter<MenuAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.menu_list_layout, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: MenuAdapter.ViewHolder, position: Int) {
        holder.bindItems(weekMenus[position])
    }

    override fun getItemCount(): Int {
        return weekMenus.size
    }

    fun addWeekMenu(weekMenus: List<WeekMenu>) {
        this.weekMenus = weekMenus
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(weekMenu: WeekMenu) = with(itemView) {
            itemView.tvMenuTitle.text = weekMenu.title

            var menuText = ""
            for (i in 0 until weekMenu.content.size) {
                menuText += weekMenu.content[i] + "\n"
            }
            itemView.tvMenuContent.text = menuText
        }
    }
}